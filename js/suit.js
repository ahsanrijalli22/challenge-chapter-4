/*
Rules: 
Gunting vs Kertas = Menang Gunting // Scissors vs Paper = Scissors Win
Kertas vs Batu    = Menang Kertas // Paper vs rock = paper win
Batu VS Gunting   = Menang Batu // rock vs scissors = rock win
Jika Semua Pilihan Sama Maka Akan Seri Atau DRAW // If all the options are the same then this game will be series
Made By Dion Arya Pamungkas
*/

function suit(your_choice) {
    /*
    Pilihan Komputer, dengan men-generate angka random 1 - 3
    1 = Gunting //Scissors
    2 = Batu  //Rock
    3 = Kertas //Paper
    */
    let hasil = "";
    const comp_choice = Math.floor(Math.random() * 3) + 1;
    let comp = "";


    switch (comp_choice) {
        case 1:
            comp = 'Gunting';
            if (your_choice == 'Kertas') {
                hasil = "PLAYER 1 LOSE";
            } else if (your_choice == 'Batu') {
                hasil = "PLAYER 1 WIN";
            } else {
                hasil = "DRAW";
            }
            break;

        case 2:
            comp = 'Batu';
            if (your_choice == 'Kertas') {
                hasil = "PLAYER 1 WIN";
            } else if (your_choice == 'Gunting') {
                hasil = "PLAYER 1 LOSE";
            } else {
                hasil = "DRAW";
            }
            break;

        case 3:
            comp = 'Kertas';
            if (your_choice == 'Batu') {
                hasil = "PLAYER 1 LOSE";
            } else if (your_choice == 'Gunting') {
                hasil = "PLAYER 1 WIN";
            } else {
                hasil = "DRAW";
            }
            break;

        default:
            hasil = "Ada Kesalahan";
            break;
    }

    var degree = "rotate(-30deg)";
    var bgcolors2 = ['#4B9653'];
    var bgcolors3 = ['#fff'];
    var px = "40px"
    var html_rotate = document.getElementById('button4');
    var html_bgcolor = document.getElementById('button4');
    var html_color = document.getElementById('hasil');
    var html_font = document.getElementById('hasil');

    html_rotate.style.transform = degree;
    html_bgcolor.style.backgroundColor = bgcolors2;
    html_color.style.color = bgcolors3;
    html_font.style.fontSize = px;
    document.getElementById('hasil').innerHTML = hasil;

    var html_hasil = document.getElementById('rock2');
    var html_hasil2 = document.getElementById('paper2');
    var html_hasil3 = document.getElementById('scissors2');

    var bgcolors = ['#C4C4C4'];
    if (comp == 'Batu') {
        html_hasil.style.backgroundColor = bgcolors;
    } else if (comp == 'Kertas') {
        html_hasil2.style.backgroundColor = bgcolors;
    } else {
        html_hasil3.style.backgroundColor = bgcolors;
    }

    var hasil_kamu1 = document.getElementById('rock');
    var hasil_kamu2 = document.getElementById('paper');
    var hasil_kamu3 = document.getElementById('scissors');

    let your = your_choice;
    if (your == 'Batu') {
        hasil_kamu1.style.backgroundColor = bgcolors;
    } else if (your == 'Kertas') {
        hasil_kamu2.style.backgroundColor = bgcolors;
    } else {
        hasil_kamu3.style.backgroundColor = bgcolors;
    }

    document.getElementById('user1').innerHTML = your_choice;
    document.getElementById('comp').innerHTML = comp;
}

function refresh() {
    location.reload(true)
}